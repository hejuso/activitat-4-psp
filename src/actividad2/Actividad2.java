package actividad2;

public class Actividad2 {
	int sumatotal = 0;
	int cont = 0;
	int media = 0;

	synchronized int nuevoValor(int valor) {

		// Sumamos los valores
		sumatotal += valor;
		// Nos quedamos por que suma vamos
		cont++;
		// Calculamos la media
		media = sumatotal / cont;

		System.out.println("Numero de veces sumado: " + cont);
		System.out.println("Numero sumado: " + sumatotal);

		// Devolvemos la media calculada
		return media;
	}
}

class caller implements Runnable {
	int valor;
	Actividad2 target;
	int sumatotal = 0;
	int media = 0;

	public caller(Actividad2 t, int v) {
		// 2- La funci��n caller ejecuta el hilo

		target = t;
		valor = v;
		new Thread(this).start();
	}

	public void run() {

		// 3- Con el synchronized le paso el valor introducido anteriormente y hago que
		// me devuelva un entero con la media ya calculada

		synchronized (target) {
			media = target.nuevoValor(valor);
		}

		// Imprimimos la emdia por pantalla
		System.out.println("Media total: " + media);
		System.out.println("");

	}
}

class Sincronizar {
	public static void main(String args[]) {

		Actividad2 target = new Actividad2();
		System.out.println("Inicio de la suma");
		System.out.println("");
		// 1- Le pasamos a la funci��n caller los parametros
		new caller(target, 5);
		new caller(target, 10);
		new caller(target, 30);
	}
}
