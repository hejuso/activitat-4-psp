package actividad1Thread;

public class Actividad1Thread extends Thread {
	public Actividad1Thread(String str) {
		super(str);
	}

	public void run() {
		for (int i = 1; i <= 5; i++) {
			try {
				System.out.println(Thread.currentThread().getName());
				sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Inicio del segundo hilo");

		Actividad1Thread hilo1 = new Actividad1Thread("main  aqui...");
		Actividad1Thread hilo2 = new Actividad1Thread("segundo hilo aqui...");

		hilo1.start();
		hilo2.start();

		hilo1.join();
		hilo2.join();

		System.out.println("Fin del segundo hilo");
	}
}