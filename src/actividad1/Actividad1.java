package actividad1;

public class Actividad1 implements Runnable {
	public void run() {

		for (int i = 1; i <= 5; i++) {
			try {
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) throws InterruptedException {
		
		Thread hilo1 = new Thread(new Actividad1(), "main  aqui...");
		Thread hilo2 = new Thread(new Actividad1(), "segundo hilo aqui...");
		System.out.println("Inicio del segundo hilo");

		hilo1.start();
		hilo2.start();
		
		hilo1.join();
		hilo2.join();
		System.out.println("Fin del segundo hilo");

	}
}
